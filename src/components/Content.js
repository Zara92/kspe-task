import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "../pages/Home";
import Movies from "../pages/Movies";
import Series from "../pages/Series";
import Error from "../pages/Error";


import classes from './Content.module.css';

const Content = () => {
    return (
        <Router>
        <div className={classes.main_container}>
                <Switch>
                    <Route exact path="/" component = {Home}></Route>
                    <Route exact path="/movies" component = {Movies}></Route>
                    <Route exact path="/series" component = {Series}></Route>
                    <Route exact path="/error" component = {Error}></Route>
                </Switch>
        </div>    
    </Router>
    )
};

export default Content;