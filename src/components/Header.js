import React from "react";
import classes from './Header.module.css';

const Header = () => {
    return (
        <>
            <header>
                <div className={classes.login_container}>
                    <h1>DEMO Streaming</h1>
                    <div className={classes.login_actions}>
                        <button type="button" className={`${classes.login_btn} ${classes.btn}`}>Log in</button>
                        <button type="button" className={`${classes.free_trail__btn} ${classes.btn}`}>Start your free trail</button>
                    </div>
                </div>
                <h2>Popular Titles</h2>
            </header>
        </>
    )
};

export default Header;