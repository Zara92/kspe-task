import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import classes from './Footer.module.css';

const Footer = () => {
    return (
        <>
            <footer>
                <Router>
                    <ul className={classes.footer_navbar}>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/">Terms and Conditions</Link>
                        </li>
                        <li>
                            <Link to="/">Privacy policy</Link>
                        </li>
                        <li>
                            <Link to="/">Collection Statment</Link>
                        </li>
                        <li>
                            <Link to="/">Help</Link>
                        </li>
                        <li>
                            <Link to="/">Manage Account</Link>
                        </li>
                    </ul>
                    <p className={classes.copywrite}>Copywrite © 2016 DEMO streaming. All Rights Reserved</p>
                </Router>
                <div className={classes.contact_wrapper} >
                    <ul className={classes.social_wrapper}>
                        <li>
                            <a href="" className={`${classes.social_icon} ${classes.facebook}`}></a>
                        </li>
                        <li>
                            <a href="" className={`${classes.social_icon} ${classes.twitter}`}></a>
                        </li>
                        <li>
                            <a href="" className={`${classes.social_icon} ${classes.instagram}`}></a>
                        </li>
                    </ul>
                    <ul className={classes.stores_wrapper}>
                        <li>
                            <a href="" className={`${classes.store_icon} ${classes.app_store}`}></a>
                        </li>
                        <li>
                            <a href="" className={`${classes.store_icon} ${classes.google_play}`}></a>
                        </li>
                        <li>
                            <a href="" className={`${classes.store_icon} ${classes.microsoft}`}></a>
                        </li>

                    </ul>
                </div>
            </footer>


        </>
    )
};

export default Footer;