import React from "react";
import {
    Link
} from "react-router-dom";

import classes from './Home.module.css';

const Home = () => {
    return (
        <>
        <div className={classes.category_wrapper}>
            <Link to="/series">
                <div className={classes.series_category}>
                    <div className={classes.category_img}>
                        <h3>Series</h3>
                    </div>
                    <p className={classes.desc}>Popular Series</p>
                </div>
            </Link>
            <Link to="/movies">
                <div className={classes.series_category}>
                    <div className={classes.category_img}>
                        <h3>Movies</h3>
                    </div>
                    <p className={classes.desc}>Popular Series</p>
                </div>
            </Link>
        </div>
        </>
    )
};

export default Home;