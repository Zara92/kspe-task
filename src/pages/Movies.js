import React from "react";

import { categoryData } from '../components/category';
import classes from './Movies.module.css';

const Movies = () => {


    const size = 21;

    const sortedArray = categoryData.entries.sort((item1, item2) => (item1.title > item2.title) ? 1 : -1);

    const filterCriteria = function (value) {
        return value.releaseYear >= 2010 && value.programType === "movie";
    }

    const filteredArray = sortedArray.filter(filterCriteria);
    return (
        <>
            <div className={classes.movies_wrapper}>
                <div className={classes.movies_outer}>
                    {filteredArray.slice(0, size).map((item, key) => {

                        const imgUrl = item.images[`Poster Art`].url;

                        return (
                            <div key={key} className={classes.movies_item}>
                                <img src={imgUrl} />
                                <p title={item.title}>{item.title}</p>
                            </div>
                        )

                    })}
                </div>
            </div>

        </>
    )
};

export default Movies;