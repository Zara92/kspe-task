import React from "react";

import { categoryData } from '../components/category';
import classes from './Series.module.css';

const Series = () => {


    const size = 21;

    const sortedArray = categoryData.entries.sort((item1, item2) => (item1.title > item2.title) ? 1 : -1);

    const filterCriteria = function (value) {
        return value.releaseYear >= 2010 && value.programType === "series";
    }

    const filteredArray = sortedArray.filter(filterCriteria);
    return (
        <>
            <div className={classes.series_wrapper}>
                <div className={classes.series_outer}>

                {filteredArray.slice(0, size).map((item, key) => {

                    const imgUrl = item.images[`Poster Art`].url;

                    return (

                        <div key={key} className={classes.series_item}>
                            <img src={imgUrl} />
                            <p title={item.title}>{item.title}</p>
                        </div>
                    )
                })}
                </div>
            </div>

        </>
    )
};

export default Series;